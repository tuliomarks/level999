﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Level999.Data.Common.Models;
using Level999.Framework.Data.Components.SQL;
using Level999.Framework.Data.Extensions;

namespace Level999.Data.Common.Controllers
{
    public class Tskcta001
    {
        #region Constructors
        #endregion

        #region Attributes
        #endregion

        #region Properties
        #endregion

        #region Events
        #endregion

        #region Methods

        public static Tska001 Insert(Tska001 tska001)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand("INSERT INTO TSKA001" +
                                     " VALUES (?ID, ?SYSA001ID, ?TITLE, ?DESCRIPTION, ?URGENCY, ?IMPORTANCE, ?TYPE, ?POINTS, NULL, ?SUN, ?MON, ?TUE, ?WED, ?THR, ?FRI, ?SAT, NULL, NOW(), 0, NULL)")
                    .AddBind("ID", tska001.Tska001Id)
                    .AddBind("SYSA001ID", tska001.Tska001Sysa001Id)
                    .AddBind("TITLE", tska001.Tska001Title)
                    .AddBind("DESCRIPTION", tska001.Tska001Description)
                    .AddBind("URGENCY", tska001.Tska001Urgency)
                    .AddBind("IMPORTANCE", tska001.Tska001Importance)
                    .AddBind("TYPE", tska001.Tska001Type)
                    .AddBind("POINTS", tska001.Tska001Points)
                    .AddBind("SUN", tska001.Tska001Sun)
                    .AddBind("MON", tska001.Tska001Mon)
                    .AddBind("TUE", tska001.Tska001Tue)
                    .AddBind("WED", tska001.Tska001Wed)
                    .AddBind("THR", tska001.Tska001Thr)
                    .AddBind("FRI", tska001.Tska001Fri)
                    .AddBind("SAT", tska001.Tska001Sat);

                sql.ExecuteQuery();               
                return null;
            }
        }

        #endregion

        public static IList<Tska001> List(long sysa001Id)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand(@"SELECT * FROM TSKA001
                                        WHERE TSKA001_SYSA001_ID = ?SYSA001ID
                                          AND DELETED = 0
                                          AND (TSKA001_DATEFINISHED IS NULL OR DATE(TSKA001_DATEFINISHED) >= DATE(NOW()))
                                        ORDER BY TSKA001_DATEFINISHED, TSKA001_ID")
                                     .AddBind("SYSA001ID", sysa001Id);

                var db = sql.ExecuteQuery();
                var list = new List<Tska001>();
                while (db.Fetch())
                {
                    list.Add(new Tska001()
                    {
                        Tska001Id = db.GetLong("TSKA001_ID"),
                        Tska001Title = db.GetString("TSKA001_TITLE"),
                        Tska001Description = db.GetString("TSKA001_DESCRIPTION"),
                        Tska001Urgency = db.GetInt ("TSKA001_URGENCY"),
                        Tska001Importance = db.GetInt("TSKA001_IMPORTANCE"),
                        Tska001Sysa001Id = db.GetLong("TSKA001_SYSA001_ID"),
                        Tska001Points = db.GetString("TSKA001_POINTS"),
                        Tska001Tska001Id = db.GetLong("TSKA001_TSKA001_ID"),
                        Tska001Type =db.GetString("TSKA001_TYPE"),
                        Tska001Sun = db.GetInt("TSKA001_SUN"),
                        Tska001Mon = db.GetInt("TSKA001_MON"),
                        Tska001Tue = db.GetInt("TSKA001_TUE"),
                        Tska001Wed = db.GetInt("TSKA001_WED"),
                        Tska001Thr = db.GetInt("TSKA001_THR"),
                        Tska001Fri = db.GetInt("TSKA001_FRI"),
                        Tska001Sat = db.GetInt("TSKA001_SAT"),
                        Tska001Datefinished = db.GetNullableDateTime("TSKA001_DATEFINISHED"),
                        DateCreated = db.GetDateTime("DATE_CREATED")
                    });
                }
                db.Close();                
                return list;
            }
        }

        public static void UpdateFinished(long id)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand(@"UPDATE TSKA001 
                                          SET TSKA001_DATEFINISHED = NOW()
                                        WHERE TSKA001_ID = ?TSKA001ID")
                    .AddBind("TSKA001ID", id);
                sql.ExecuteNonQuery();

            }
        }

        public static void UpdateDeleted(long id)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand(@"UPDATE TSKA001 
                                          SET DELETED = 1,
                                              DATE_DELETED =  NOW()
                                        WHERE TSKA001_ID = ?TSKA001ID")
                    .AddBind("TSKA001ID", id);
                sql.ExecuteNonQuery();

            }
        }
    }
}
