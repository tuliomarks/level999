﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Level999.Data.Common.Models;
using Level999.Framework.Data.Components.SQL;
using Level999.Framework.Data.Extensions;

namespace Level999.Data.Common.Controllers
{
    public static class Syscta001
    {
        #region Constructors
        #endregion

        #region Attributes
        #endregion

        #region Properties
        #endregion

        #region Events
        #endregion

        #region Methods

        public static Sysa001 Select(string user)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand("SELECT * FROM SYSA001" +
                                     " WHERE SYSA001_USER = ?USER")
                    .AddBind("USER", user);

                var rd = sql.ExecuteQuery();
                if (rd.Fetch())
                {
                    return new Sysa001()
                    {
                        Sysa001Id = rd.GetLong("SYSA001_ID"),
                        Sysa001Name = rd.GetString("SYSA001_NAME"),
                        Sysa001Email = rd.GetString("SYSA001_EMAIL"),
                        Sysa001Password = rd.GetString("SYSA001_PASSWORD"),
                        Sysa001User = rd.GetString("SYSA001_USER"),
                    };
                }
                return null;
            }
        }

        public static Sysa001 SelectByEmail(string email)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand("SELECT * FROM SYSA001" +
                                     " WHERE SYSA001_EMAIL = ?EMAIL")
                    .AddBind("EMAIL", email);

                var rd = sql.ExecuteQuery();
                if (rd.Fetch())
                {
                    return new Sysa001()
                    {
                        Sysa001Id = rd.GetLong("SYSA001_ID"),
                        Sysa001Name = rd.GetString("SYSA001_NAME"),
                        Sysa001Email = rd.GetString("SYSA001_EMAIL"),
                        Sysa001Password = rd.GetString("SYSA001_PASSWORD"),
                        Sysa001User = rd.GetString("SYSA001_USER"),
                    };
                }
                return null;
            }
        }

        public static Sysa001 Insert(Sysa001 sysa001)
        {
            using (var sql = new Sql())
            {
                sql.AggregateCommand(@"INSERT INTO SYSA001
                                     VALUES (?ID, ?NAME, ?EMAIL, ?USER, ?PASSWORD, NOW(), 0)")
                    .AddBind("ID", sysa001.Sysa001Id)
                    .AddBind("NAME", sysa001.Sysa001Name)
                    .AddBind("EMAIL", sysa001.Sysa001Email)
                    .AddBind("USER", sysa001.Sysa001User)
                    .AddBind("PASSWORD", sysa001.Sysa001Password);

                sql.ExecuteQuery();
                return null;
            }
        }

        #endregion
    }
}
