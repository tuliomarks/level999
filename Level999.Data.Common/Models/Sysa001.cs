﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Level999.Data.Common.Models
{
    public class Sysa001
    {
        #region Constructors
        #endregion

        #region Attributes
        #endregion

        #region Properties
        public long Sysa001Id { get; set; }
        public string Sysa001Name { get; set; }
        public string Sysa001Email { get; set; }
        public string Sysa001User { get; set; }
        public string Sysa001Password { get; set; }
        #endregion

        #region Events
        #endregion

        #region Methods
        #endregion

    }
}
