﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Level999.Data.Common.Models
{
    public class Tska001
    {
        #region Constructors
        #endregion

        #region Attributes
        #endregion

        #region Properties
        public long Tska001Id { get; set; }
        public long Tska001Sysa001Id { get; set; }
        public string Tska001Title { get; set; }
        public string Tska001Description { get; set; }
        public int Tska001Urgency { get; set; }
        public int Tska001Importance { get; set; }
        public string Tska001Type { get; set; }
        public string Tska001Points { get; set; }
        public long Tska001Tska001Id { get; set; }
        public int Tska001Sun { get; set; }
        public int Tska001Mon { get; set; }
        public int Tska001Tue { get; set; }
        public int Tska001Wed { get; set; }
        public int Tska001Thr { get; set; }
        public int Tska001Fri { get; set; }
        public int Tska001Sat { get; set; }
        public DateTime? Tska001Datefinished { get; set; }
        public DateTime DateCreated { get; set; }
        public int Deleted { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Events
        #endregion

        #region Methods
        #endregion
    }
}
