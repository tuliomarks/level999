﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Level999.Data.Common.Controllers;
using Level999.Data.Common.Models;

namespace Level999.Business.Controllers
{
    public class Sysbsa001
    {
        /// <summary>
        /// Inserts an user (SYSA001)
        /// </summary>
        public static Sysa001 Sysa001001(string name, string email)
        {

            if (Syscta001.SelectByEmail(email) != null)
            {
                throw new Exception("O email informado já esta sendo utilizado.");
            }

            var sysa001 = new Sysa001();
            sysa001.Sysa001Name = name;
            sysa001.Sysa001Email = email;
            sysa001.Sysa001User = email;

            var rnd = new Random();
            const string pool = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var chars = Enumerable.Range(0, 12).Select(x => pool[rnd.Next(0, pool.Length)]);

            sysa001.Sysa001Password = new string(chars.ToArray());

            Syscta001.Insert(sysa001);
            return sysa001;

        }
    }
}
