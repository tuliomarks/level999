﻿using System.Web.Mvc;

namespace Level999.Web.Components.Filters
{
    public class AuthenticationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionDescriptor.ActionName != "InternalServer" &&
                filterContext.ActionDescriptor.ActionName != "NotFound" &&
                filterContext.ActionDescriptor.ActionName != "Forbidden" &&
                filterContext.ActionDescriptor.ActionName != "Login" &&
                !(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Enterprise" && filterContext.ActionDescriptor.ActionName == "Entrance")&&
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Console" &&
                LocalSessionContext.GetCurrent().User == null)
            {
                // guarda a URL para retorno apos logar
                LocalSessionContext.GetCurrent().UrlReturn = filterContext.HttpContext.Request.RawUrl;

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new HttpUnauthorizedResult("A sessão atual expirou.");
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/Home/Login");
                }
            }     
        }     

    }
}