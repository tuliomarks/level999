﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Level999.Data.Common.Models;

namespace Level999.Web.Components
{
    public class LocalSessionContext
    {

        public string UrlReturn { get; set; }
        public Sysa001 User { get; set; }

        public new static LocalSessionContext GetCurrent()
        {
            var session = (LocalSessionContext)HttpContext.Current.Session["__SessionContext__"];
            if (session == null)
            {
                session = new LocalSessionContext();
                HttpContext.Current.Session["__SessionContext__"] = session;
            }
            return session;
        }

        public static void ClearCurrent()
        {
            HttpContext.Current.Session.Clear();
        }
    }
}