﻿using System.Web.Optimization;

namespace Level999.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css/defaults").Include("~/Content/css/bootstrap.min.css",
                                                                          "~/Content/css/materialadmin.min.css",
                                                                          "~/Content/css/libs/animate/animate.css",
                                                                          "~/Content/css/ludotask.css",
                                                                          "~/Content/css/libs/toastr/toastr.css"));

            bundles.Add(new StyleBundle("~/Content/css/fonts").Include("~/Content/css/font-awesome.min.css",
                                                                       "~/Content/css/material-design-iconic-font.min.css"));

            bundles.Add(new StyleBundle("~/Content/js/material").Include("~/Content/js/core/source/App.js",
                                                                         "~/Content/js/core/source/AppCard.js",
                                                                         "~/Content/js/core/source/AppNavigation.js",
                                                                         "~/Content/js/core/source/AppNavSearch.js",
                                                                         "~/Content/js/core/source/AppOffcanvas.js",
                                                                         "~/Content/js/core/source/AppForm.js",
                                                                         "~/Content/js/libs/toastr/toastr.js",
                                                                         "~/Content/js/libs/angular-typed/angular-typed.js"));

        }
    }
}