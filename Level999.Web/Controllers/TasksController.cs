﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Level999.Data.Common.Models;
using Level999.Web.Components;
using Microsoft.Ajax.Utilities;
using Level999.Data.Common.Controllers;

namespace Level999.Web.Controllers
{
    public class TasksController : BaseController
    {
        // GET: Tasks
        [HttpPost]
        public ActionResult Insert(string title, string description, string urgency, string importance, bool? sun, bool? mon, bool? tue, bool? wed, bool? thr, bool? fri, bool? sat)
        {
            if (string.IsNullOrEmpty(title))
                return Json(new { status = "FAIL", field = "title", message=  "Ei! Você precisa informar um título para a tarefa."});

            //if (string.IsNullOrEmpty(description))
            //    return Json(new { status = "FAIL", field = "description" });

            if (string.IsNullOrEmpty(urgency))
                urgency = "0";
            //return Json(new { status = "FAIL", field = "urgency" });

            if (string.IsNullOrEmpty(importance))
                importance = "0";
            //return Json(new { status = "FAIL", field = "importance" });

            int auxUrgency = int.MinValue;
            if (!int.TryParse(urgency, out auxUrgency))
                auxUrgency = 0;
            //return Json(new { status = "FAIL", field = "urgency" });

            int auxImportance = int.MinValue;
            if (!int.TryParse(importance, out auxImportance))
                auxImportance = 0;
            //return Json(new { status = "FAIL", field = "importance" });


            var tska001 = new Tska001()
            {
                Tska001Sysa001Id = LocalSessionContext.GetCurrent().User.Sysa001Id,
                Tska001Title = title,
                Tska001Description = description,
                Tska001Urgency = auxUrgency,
                Tska001Importance = auxImportance,
                Tska001Sun = sun.HasValue ? 1 : 0,
                Tska001Mon = mon.HasValue ? 1 : 0,
                Tska001Tue = tue.HasValue ? 1 : 0,
                Tska001Wed = wed.HasValue ? 1 : 0,
                Tska001Thr = thr.HasValue ? 1 : 0,
                Tska001Fri = fri.HasValue ? 1 : 0,
                Tska001Sat = sat.HasValue ? 1 : 0,
                Tska001Points = "0",
                Tska001Type = "D"
            };

            Tskcta001.Insert(tska001);

            return Json(new { status = "OK", model = tska001 });
        }

        public ActionResult List()
        {
            var taskList = Tskcta001.List(LocalSessionContext.GetCurrent().User.Sysa001Id);

            return Json(new { status = "OK", list = taskList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Finish(long id)
        {
            Tskcta001.UpdateFinished(id);

            return Json(new { status = "OK", dateFinished = DateTime.Now }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            Tskcta001.UpdateDeleted(id);

            return Json(new { status = "OK" }, JsonRequestBehavior.AllowGet);
        }
    }
}
