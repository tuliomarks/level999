﻿using System.Web.Mvc;
using Level999.Data.Common.Controllers;
using Level999.Web.Components;

namespace Level999.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string user, string pass)
        {
            Logout();

            var sysa001 = Syscta001.Select(user);
            if (sysa001 != null)
            {
                if (sysa001.Sysa001Password != pass)
                    return Json(new { status = "FAIL" });

                LocalSessionContext.GetCurrent().User = sysa001;
                return Json(new { status = "OK", redirect = Url.Action("Index") });
            }
            return Json(new { status = "FAIL" });
        }

        public ActionResult GetProfile()
        {
            var user = LocalSessionContext.GetCurrent().User;
            user.Sysa001Password = null;
            return Json(new { status = "OK", data = user }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            if (LocalSessionContext.GetCurrent().User != null)
            {
                LocalSessionContext.GetCurrent().User = null;
            }
            return Json(new { status = "OK" });
        }
    }
}
