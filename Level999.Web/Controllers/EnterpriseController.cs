﻿using System;
using System.Web.Mvc;
using Level999.Business.Controllers;
using Level999.Data.Common.Controllers;
using Level999.Web.Components;

namespace Level999.Web.Controllers
{
    public class EnterpriseController : BaseController
    {
        public ActionResult Entrance()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Entrance(string name, string email)
        {

            try
            {
                var sysa001 = Sysbsa001.Sysa001001(name, email);

                LocalSessionContext.GetCurrent().User = sysa001;
                return Json(new { status = "OK", redirect = Url.Action("Index", "Home") });
            }
            catch (Exception e)
            {
                return Json(new { status = "FAIL", message = e.Message });
            }                       
        }
    }
}
