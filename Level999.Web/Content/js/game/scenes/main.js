/*! level999 v1.0.0 | by Tulio Marks | 10/10/2017 */
/**
 * Define home/index scene
 * 
 * @returns {} 
 */
function Scene() { };
Scene.prototype = {
    renderScale: 1,
    floors: [],
    preload: function () {

        $g.factory.floor.preload();
        $g.factory.vehicles.preload();

        //$g.game.load.spritesheet('player', 'content/img/game/player_office.png', 64, 64, 12);
    },
    create: function () {
        // scalling 
        //$g.game.scale.pageAlignHorizontally = true;
        //$g.game.scale.pageAlignVertically = true;
        $g.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        $g.game.stage.disableVisibiltyChange = true;

        $g.game.stage.backgroundColor = '#9ABDD7';

        // initialize first floor
        var playerLevel = 3;
        if (playerLevel <= 2) {
             var base = $g.factory.floor.createBase();
        } else {
            var base = $g.factory.floor.createInitial(playerLevel);
        }
        this.floors[0] = base.floor;
        this.renderScale = base.renderScale;
        
        // initialize other floors        
        var i;
        for (i = 1; i < 3; i++) {
            this.floors[i] = $g.factory.floor.createByLevel(playerLevel, this.floors[i - 1], this.renderScale);
        }

        // initialize vehicles
        if (playerLevel <= 2)
        $g.factory.vehicles.createRandom(this.floors[0]);

        // fazer random da velocidade e do delay ... assinar evento e iniciar denovo
        //https://phaser.io/docs/2.6.2/Phaser.Tween.html#onComplete

        //  Creates a layer from the World1 layer in the map data.
        //  A Layer is effectively like a Phaser.Sprite, so is added to the display list.
        //var floorLayer = map.createLayer('Floor');
        //floorLayer.resizeWorld();

        this.cursors = $g.game.input.keyboard.createCursorKeys();

        // faz um zoom in
        //$g.game.world.scale.set(2);

        // posiciona a camera no meio
        //$g.game.camera.x = ($g.game.width * 0.1);
        //$g.game.camera.y = ($g.game.height * 0.5);

    },
    update: function () {
        if (this.cursors.left.isDown) {
            $g.game.camera.x -= 8;
        }
        else if (this.cursors.right.isDown) {
            $g.game.camera.x += 8;
        }

        if (this.cursors.up.isDown) {
            $g.game.camera.y -= 8;
        }
        else if (this.cursors.down.isDown) {
            $g.game.camera.y += 8;
        }
    },
    render: function () {

    },
    updateText: function () {
        this.text.setText("Level " + player.getLevel() + " - " + player.points);
    },
    createText: function () {
        var text = $g.game.add.text(0, 0, "1000");
        text.setTextBounds(0, 16, 580, 768);

        text.font = 'Skranji';
        text.fontSize = 20;
        text.fill = "#ffffff";
        text.align = 'center';
        text.boundsAlignV = 'top';
        text.boundsAlignH = "right";
        this.hudLayer.add(text);
        this.text = text;
        this.updateText();
    }   
};


