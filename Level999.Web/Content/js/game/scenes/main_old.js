/**
     * Define home/index scene
     * 
     * @returns {} 
     */
function Scene() { };
Scene.prototype = {
    renderScale: 1,
    preload: function () {
        $g.game.load.image('floorBase', 'content/img/game/base1.png');
        $g.game.load.image('floor1', 'content/img/game/floor1.png');
        $g.game.load.image('floor2', 'content/img/game/floor2.png');
        $g.game.load.image('floor3', 'content/img/game/floor3.png');
        $g.game.load.image('furniture1', 'content/img/game/furniture1.png');
        $g.game.load.image('vehicle1', 'content/img/game/vehicle1.png');
        $g.game.load.image('vehicle2', 'content/img/game/vehicle2.png');
        $g.game.load.image('vehicle3', 'content/img/game/vehicle3.png');
        $g.game.load.image('vehicle4', 'content/img/game/vehicle4.png');
        $g.game.load.image('vehicle5', 'content/img/game/vehicle5.png');
        $g.game.load.image('vehicle6', 'content/img/game/vehicle6.png');

        //$g.game.load.spritesheet('player', 'content/img/game/player_office.png', 64, 64, 12);
    },
    create: function () {
        // scalling 
        $g.game.scale.pageAlignHorizontally = true;
        $g.game.scale.pageAlignVertically = true;
        $g.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        $g.game.stage.backgroundColor = '#9ABDD7';

        var floorBase = $g.game.add.sprite(0, 0, 'floorBase');
        this.renderScale = $g.game.world.width / floorBase.width;
        floorBase.scale.set(this.renderScale, this.renderScale);
        floorBase.y = $g.game.world.height - floorBase.height;

        var floor1 = $g.game.add.sprite(0, 0, 'floor3').alignTo(floorBase, Phaser.TOP, 0, -23);
        floor1.scale.set(this.renderScale, this.renderScale);

        var furniture1 = $g.game.add.sprite(0, 0, 'furniture1').alignTo(floorBase, Phaser.TOP, 0, -23);
        furniture1.scale.set(this.renderScale, this.renderScale);

        var floor2 = $g.game.add.sprite(0, 0, 'floor2').alignTo(floor1, Phaser.TOP, 0, -23);
        floor2.scale.set(this.renderScale, this.renderScale);

        var furniture2 = $g.game.add.sprite(0, 0, 'furniture1').alignTo(floor1, Phaser.TOP, 0, -23);
        furniture2.scale.set(this.renderScale, this.renderScale);

        var floor3 = $g.game.add.sprite(0, 0, 'floor2').alignTo(floor2, Phaser.TOP, 0, -23);
        floor3.scale.set(this.renderScale, this.renderScale);

        console.log(this);
        this.initTraffic(floorBase);

        // fazer random da velocidade e do delay ... assinar evento e iniciar denovo
        //https://phaser.io/docs/2.6.2/Phaser.Tween.html#onComplete

        //  Creates a layer from the World1 layer in the map data.
        //  A Layer is effectively like a Phaser.Sprite, so is added to the display list.
        //var floorLayer = map.createLayer('Floor');
        //floorLayer.resizeWorld();

        this.cursors = $g.game.input.keyboard.createCursorKeys();

        // faz um zoom in
        //$g.game.world.scale.set(2);

        // posiciona a camera no meio
        //$g.game.camera.x = ($g.game.width * 0.1);
        //$g.game.camera.y = ($g.game.height * 0.5);

    },
    update: function () {
        if (this.cursors.left.isDown) {
            $g.game.camera.x -= 8;
        }
        else if (this.cursors.right.isDown) {
            $g.game.camera.x += 8;
        }

        if (this.cursors.up.isDown) {
            $g.game.camera.y -= 8;
        }
        else if (this.cursors.down.isDown) {
            $g.game.camera.y += 8;
        }
    },
    render: function () {

    },
    updateText: function () {
        this.text.setText("Level " + player.getLevel() + " - " + player.points);
    },
    createText: function () {
        var text = $g.game.add.text(0, 0, "1000");
        text.setTextBounds(0, 16, 580, 768);

        text.font = 'Skranji';
        text.fontSize = 20;
        text.fill = "#ffffff";
        text.align = 'center';
        text.boundsAlignV = 'top';
        text.boundsAlignH = "right";
        this.hudLayer.add(text);
        this.text = text;
        this.updateText();
    },
    initTraffic: function (floor) {

        var vehicles = [{ sprite: 'vehicle1', position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                        { sprite: 'vehicle2', position: Phaser.BOTTOM_LEFT, xAlign: 700, yAlign: -230 },
                        { sprite: 'vehicle3', position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                        { sprite: 'vehicle4', position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                        { sprite: 'vehicle5', position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                        { sprite: 'vehicle6', position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                        { sprite: 'vehicle1', position: Phaser.BOTTOM_RIGHT, xAlign: 1200, yAlign: -300, reverse: true },
                        { sprite: 'vehicle2', position: Phaser.BOTTOM_RIGHT, xAlign: 2000, yAlign: -500, reverse: true },
                        { sprite: 'vehicle3', position: Phaser.BOTTOM_RIGHT, xAlign: 1500, yAlign: -300, reverse: true },
                        { sprite: 'vehicle4', position: Phaser.BOTTOM_RIGHT, xAlign: 1200, yAlign: -300, reverse: true },
                        { sprite: 'vehicle5', position: Phaser.BOTTOM_RIGHT, xAlign: 1200, yAlign: -300, reverse: true },
                        { sprite: 'vehicle6', position: Phaser.BOTTOM_RIGHT, xAlign: 1500, yAlign: -300, reverse: true }];
        var spriteRnd = $g.game.rnd.integerInRange(0, 11);

        var model = vehicles[spriteRnd];
        var vehicle = $g.game.add.sprite(0, 0, model.sprite).alignTo(floor, model.position, model.xAlign, model.yAlign);
        vehicle.scale.set(this.renderScale, this.renderScale);

        // flip horizontal
        var to = { x: $g.game.world.width + vehicle.width };
        if (model.reverse) {
            vehicle.scale.x *= -1;
            to = { x: 0 };
        }

        var tweenTime = $g.game.rnd.integerInRange(1000, 2500);
        var tween = $g.game.add.tween(vehicle).to(to, tweenTime, Phaser.Easing.Linear.None);
        tween.onComplete.add(function (target, tween) {
            target.destroy();
            $g.curScene.initTraffic(floor);
        }, true);
        tween.start();
    }
};

/*! level999 v1.0.0 | by Tulio Marks | 10/10/2017 */
var $g = {};
(function ($, $g) {
    'use strict';

    /**
     * Define a Player class
     * 
     * @returns {} 
     */
    $g.player = function() {
        $g.player.points = 0;
        $g.player.getLevel = function () {
            return parseInt(this.points / 50); // L(n) = 50 * n;
        }
    };    

    $g.init = function () {
        $g.debug = false;        
        if (Scene === undefined) {
            console.log("Scene class definition is missing!");
            return;
        }
        $g.curScene = new Scene();
        $g.game.state.add("PlayGame", $g.curScene);
        $g.game.state.start("PlayGame");
    };
})(jQuery, $g);

window.onload = function () {
    $g.game = new Phaser.Game($("#level999-game").width(), $("#level999-game").height(), Phaser.CANVAS, 'level999-game');
    $g.init();
};


