﻿/*! level999 v1.0.0 | by Tulio Marks | 10/10/2017 */
(function ($, $g) {
    'use strict';
    $g.factory = {};
    $g.factory.floor = {
        images: {
            baseFloors: ['floorBase'],
            floors: ['floor1', 'floor2', 'floor3', 'floor4', 'floor999'],
            furnitures: ['furniture1', 'furniture3', 'furniture4'],
        },
        preload: function () {
            var images = [];
            images.push.apply(images, this.images.floors);
            images.push.apply(images, this.images.baseFloors);
            images.push.apply(images, this.images.furnitures);
            $.each(images, function (idx, value) {
                $g.game.load.image(value, 'content/img/game/' + value + '.png');
            });
        },
        createBase: function () {
            var idx = $g.game.rnd.integerInRange(0, this.images.baseFloors.length - 1);
            var floor = $g.game.add.sprite(0, 0, this.images.baseFloors[idx]);

            var renderScale = $g.game.world.width / floor.width;
            floor.scale.set(renderScale, renderScale);

            floor.y = $g.game.world.height - floor.height;

            return { floor, renderScale };
        },
        createInitial: function (level) {
            var idx = 0;
            if (level > 6)
                idx = 1;
            if (level > 11)
                idx = 2;
            if (level > 16)
                idx = 3;

            var floor = $g.game.add.sprite(0, 0, this.images.floors[idx]);

            var renderScale = $g.game.world.width / floor.width;
            floor.scale.set(renderScale, renderScale);
           
            floor.alignIn($g.game.world.bounds, Phaser.CENTER);

            $g.factory.floor.createFurniture(floor, renderScale);

            var orientationFloor = floor;
            for (var i = 1; i < 3; i++) {
                orientationFloor = $g.factory.floor.createRandom(orientationFloor, renderScale, 'down');
            }

            return { floor, renderScale };
        },
        createByLevel: function (level, priorFloor, renderScale, direction) {
            var idx = 0;
            if (level > 6)
                idx = 1;
            if (level > 11)
                idx = 2;
            if (level > 16)
                idx = 3;

            var floor = $g.game.add.sprite(0, 0, this.images.floors[idx]);

            if (direction === "down") {
                floor.alignTo(priorFloor, Phaser.BOTTOM_LEFT, 0, 0);
            } else {
                floor.alignTo(priorFloor, Phaser.TOP, 0, -23);
            }

            if (renderScale)
                floor.scale.set(renderScale, renderScale);

            $g.factory.floor.createFurniture(level, floor, renderScale);

            return floor;
        },
        createRandom: function (priorFloor, renderScale, direction) {
            var idx = $g.game.rnd.integerInRange(0, this.images.floors.length - 1);
            var floor = $g.game.add.sprite(0, 0, this.images.floors[idx]);

            if (direction === "down") {
                floor.alignTo(priorFloor, Phaser.BOTTOM_LEFT, 0, 0);
            } else {
                floor.alignTo(priorFloor, Phaser.TOP, 0, -23);
            }
            
            if (renderScale)
                floor.scale.set(renderScale, renderScale);

            $g.factory.floor.createFurniture(floor, renderScale);

            return floor;
        },
        createFurniture: function (floor, renderScale) {       

            var idx = $g.game.rnd.integerInRange(0, this.images.furnitures.length - 1);
            var furniture = $g.game.add.sprite(0, 0, this.images.furnitures[idx]).alignIn(floor, Phaser.LEFT_TOP);
            if (renderScale)
                furniture.scale.set(renderScale, renderScale);
        },
        createFurnitureByLevel: function (level, floor, renderScale) {       
            var idx = 0;
            if (level > 3)
                idx = 1;
            if (level > 8)
                idx = 2;
            if (level > 12)
                idx = 3;
            if (level > 17)
                idx = 4;
       
            var furniture = $g.game.add.sprite(0, 0, this.images.furnitures[idx]).alignIn(floor, Phaser.LEFT_TOP);
            if (renderScale)
                furniture.scale.set(renderScale, renderScale);
        }

};
$g.factory.vehicles = {
    images: {
        vehicles: ['vehicle1', 'vehicle2', 'vehicle3', 'vehicle4', 'vehicle5', 'vehicle6'],
    },
    preload: function () {
        var images = [];
        images.push.apply(images, this.images.vehicles);
        $.each(images, function (idx, value) {
            $g.game.load.image(value, 'content/img/game/' + value + '.png');
        });
    },
    createRandom: function (floor) {
        var models = [{ sprite: this.images.vehicles[0], position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                    { sprite: this.images.vehicles[1], position: Phaser.BOTTOM_LEFT, xAlign: 700, yAlign: -230 },
                    { sprite: this.images.vehicles[2], position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                    { sprite: this.images.vehicles[3], position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                    { sprite: this.images.vehicles[4], position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                    { sprite: this.images.vehicles[5], position: Phaser.BOTTOM_LEFT, xAlign: 600, yAlign: -120 },
                    { sprite: this.images.vehicles[0], position: Phaser.BOTTOM_RIGHT, xAlign: 1200, yAlign: -300, reverse: true },
                    { sprite: this.images.vehicles[1], position: Phaser.BOTTOM_RIGHT, xAlign: 2000, yAlign: -500, reverse: true },
                    { sprite: this.images.vehicles[2], position: Phaser.BOTTOM_RIGHT, xAlign: 1500, yAlign: -300, reverse: true },
                    { sprite: this.images.vehicles[3], position: Phaser.BOTTOM_RIGHT, xAlign: 1200, yAlign: -300, reverse: true },
                    { sprite: this.images.vehicles[4], position: Phaser.BOTTOM_RIGHT, xAlign: 1200, yAlign: -300, reverse: true },
                    { sprite: this.images.vehicles[5], position: Phaser.BOTTOM_RIGHT, xAlign: 1500, yAlign: -300, reverse: true }];
        var spriteRnd = $g.game.rnd.integerInRange(0, 11);

        var model = models[spriteRnd];

        var vehicle = $g.game.add.sprite(0, 0, model.sprite).alignTo(floor, model.position, model.xAlign, model.yAlign);
        if ($g.curScene.renderScale !== undefined)
            vehicle.scale.set($g.curScene.renderScale, $g.curScene.renderScale);

        // flip horizontal
        var to = { x: $g.game.world.width + vehicle.width };
        if (model.reverse) {
            vehicle.scale.x *= -1;
            to = { x: 0 };
        }

        var tweenTime = $g.game.rnd.integerInRange(1000, 2500);
        var tween = $g.game.add.tween(vehicle).to(to, tweenTime, Phaser.Easing.Linear.None);
        tween.onComplete.add(function (target, tween) {
            target.destroy();
            $g.factory.vehicles.createRandom(floor);
        }, true);
        tween.start();

        return vehicle;
    }
}

})(jQuery, $g);