﻿/*! level999 v1.0.0 | by Tulio Marks | 10/10/2017 */
var $g = {};
(function ($, $g) {
    'use strict';

    $g.init = function (game) {
        $g.game = game;
        $g.debug = false;
        if (Scene === undefined) {
            console.log("Scene class definition is missing!");
            return;
        }
        $g.curScene = new Scene();
        $g.game.state.add("PlayGame", $g.curScene);
        $g.game.state.start("PlayGame");
    };
})(jQuery, $g);