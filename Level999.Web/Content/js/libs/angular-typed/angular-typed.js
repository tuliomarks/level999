﻿angular.module('angularTyped', [])
    .directive('typed', ['$log', '$timeout', '$q', '$compile', function ($log, $timeout, $q, $compile) {
        return {
            scope: {
                typeStrings: '=?',
                startCallback: '&',
                endCallback: '&'
            },
            restrict: 'AE',
            replace: false,
            link: function (scope, elem, attrs) {

                scope.container = elem;
                // initialize attributes
                scope.model = {
                    strings: scope.typeStrings || (scope.container.text() ? [scope.container.text().trim().replace(/\s+/g, ' ')] : null),
                    startLine: attrs.startLine || 0,
                    htmlMode: typeof attrs.htmlMode !== 'undefined',
                    startTimeout: parseInt(attrs.startTimeout) || 0,
                    endlineTimeout: parseInt(attrs.endlineTimeout) || 1000,
                    loop: 'loop' in attrs && attrs.loop != 'false',
                    typeSpeed: parseInt(attrs.typeSpeed) || 0,
                    backSpeed: parseInt(attrs.backSpeed) || 0,
                    removeLine: attrs.removeLine || true,
                    removeLast: attrs.removeLast || false
                }
                scope.init = function () {
                    // if no strings exit
                    if (scope.model.strings === null || !scope.model.strings.length) {
                        $log.info('angular-typed: No strings to type found on element: ', scope.containe[0]);
                        return;
                    }

                    scope.initContainer();

                    // if valid function
                    if (angular.isFunction(scope.startCallback)) {
                        scope.startCallback();
                    }

                    scope.startTyping();
                };
                scope.initContainer = function () {

                    scope.cursorElement = angular.element(document.createElement('span')).addClass('typed-cursor').text('|');

                    // clear element
                    scope.container.html('');
                    // append cursor
                    scope.container.append(scope.cursorElement);
                    // create the first text span
                    scope.newLine();
                };
                /**
                 * Create a new line for the text to type
                 */
                scope.newLine = function () {
                    if (scope.currentElementLine) {
                        scope.currentElementLine.attr("style", "display: block;");
                    }
                    scope.renderText();
                    // set scroll to bottom
                    window.scrollTo(0, scope.container[0].scrollHeight +50);
                };
                /**
                 * Starts all typing flow 
                 */
                scope.startTyping = function () {
                    $timeout(function () {
                        // reset current line with original start line, in nextText() will increment it
                        scope.currentLine = scope.model.startLine - 1;
                        // start everything
                        return scope.nextText();
                    }, scope.model.startTimeout);
                };
                /**
                 * Select next text model and start typing it
                 */
                scope.nextText = function (disableStartDelay) {                  

                    scope.currentLine++;
                    var startPos = 0;
                    var itemText = scope.model.strings[scope.currentLine];
                    scope.currentLineItem = itemText;

                    var timeout = itemText.endlineTimeout ? itemText.endlineTimeout : scope.model.endlineTimeout;
                    if (scope.currentLine === scope.model.startLine || disableStartDelay) {
                        timeout = 0;
                    }

                    $timeout(function () {
                        if (typeof itemText === "string") {
                            return scope.typeText(itemText, startPos);
                        } else {
                            if (itemText.message) {
                                return scope.typeText(itemText.message, startPos);
                            }else if (itemText.input) {
                                return scope.renderInput(itemText);
                            } else if (itemText.select) {
                                return scope.renderSelect(itemText);
                            }
                        }
                    }, timeout);
                }
                /**
                 * Type some text in the container
                 */
                scope.typeText = function (text, currPos) {

                    // get a random type speed delay
                    var delay = Math.round(Math.random() * (100 - 30)) + scope.model.typeSpeed;

                    $timeout(function () {

                        var charPause = 0;
                        var substr = text.substr(currPos);

                        // check for any user defined pause
                        if (substr.charAt(0) === '^') {

                            var skip = 1; // skip atleast 1

                            if (/^\^\d+/.test(substr)) {
                                substr = /\d+/.exec(substr)[0];
                                skip += substr.length;
                                charPause = parseInt(substr);
                            }

                            // strip out the escape character and pause value so they're not printed
                            text = text.substring(0, currPos) + text.substring(currPos + skip);
                        }

                        if (scope.model.htmlMode) {

                            var curChar = text.substr(currPos).charAt(0);

                            if (curChar === '<' || curChar === '&') {

                                var tag = '';
                                var endtag = (curChar === '<') ? '>' : ';';

                                while (text.substr(currPos).charAt(0) !== endtag) {
                                    tag += text.substr(currPos).charAt(0);
                                    currPos++;
                                }
                            }
                        }
                        /**
                         * A ipotetical character pause defined by user (by default 0)
                         */
                        $timeout(function () {

                            // if end of current string
                            if (currPos == text.length) {

                                /**
                                * At end of all lines run the end-callback
                                */
                                if (scope.currentLine == scope.model.strings.length - 1) {

                                    // check if last line should be removed
                                    if (scope.model.removeLast === true) {
                                        scope.typeBackspace(text, currPos);
                                    } else {

                                        // At the end of all run the callback if
                                        // specified and check if loop mode is enabled
                                        if (angular.isFunction(scope.endCallback)) {
                                            // apply the user callback
                                            scope.$apply(scope.endCallback());
                                        }

                                        // if loop mode is enabled
                                        // reset currentLine to starting line
                                        // and loop it !
                                        return scope.model.loop ? scope.startTyping() : null;
                                    }

                                } else {
                                    if (scope.model.removeLine === true || (scope.currentLineItem.removeLine != null && scope.currentLineItem.removeLine === true)) {
                                        scope.typeBackspace(text, currPos);
                                    } else {                                       
                                        scope.newLine();
                                        return scope.nextText();
                                    }
                                }
                                /**
                                * Move to the next character that need to be typed (still the same line)
                                */
                            } else {
                                var nextPos = currPos + 1;
                                var string = text.substr(0, nextPos);

                                // write new text
                                scope.model.htmlMode ? scope.currentElementLine.html(string) : scope.currentElementLine.text(string);

                                // set scroll
                                window.scrollTo(0, scope.container[0].scrollHeight);

                                // type next char
                                scope.typeText(text, nextPos);
                            }
                        }, charPause);
                    }, delay);
                };
                /**
                * Simulate the backspace press for erasing text
                */
                scope.typeBackspace = function (text, currPos) {

                    // get a random type speed delay
                    var delay = Math.round(Math.random() * (100 - 30)) + scope.model.backSpeed;

                    $timeout(function () {
                        if (scope.model.htmlMode) {
                            var curChar = text.substr(currPos).charAt(0);
                            if (curChar === '>' || curChar === ';') {

                                var tag = '';
                                var endtag = (curChar === '>') ? '<' : '&';

                                while (text.substr(currPos).charAt(0) !== endtag) {
                                    tag += text.substr(currPos).charAt(0);
                                    currPos--;
                                }
                            }
                        }
                        if (currPos == 0) {

                            if (scope.currentLine < scope.model.strings.length - 1) {
                                return scope.nextText();
                            } else {

                                // At the end of all run the callback if
                                // specified and check if loop mode is enabled
                                if (angular.isFunction(scope.endCallback)) {
                                    // apply the user callback
                                    scope.$apply(scope.endCallback());
                                }

                                // if loop mode is enabled
                                // reset currentLine to starting line
                                // and loop it !
                                return scope.model.loop ? scope.startTyping() : null;

                            }
                        } else {
                            var nextPos = currPos - 1;
                            var string = text.substr(0, nextPos);

                            // write new text
                            scope.model.htmlMode ? scope.currentElementLine.html(string) : scope.currentElementLine.text(string);

                            // type next
                            scope.typeBackspace(text, nextPos);
                        };
                    }, delay);
                }
                scope.renderText = function(text, alignRight) {
                    var line = angular.element('<span class="typed-line ' + alignRight + ' animate-left"></span>');
                    if (text) {
                        line.text(text);
                    }                    
                    scope.cursorElement.before(line);
                    scope.currentElementLine = line;
                }
                scope.renderInput = function (item) {

                    var template = '<div class="form-group input-group typed-input animate-left floating-label"> ' +
                        '<input type="text" class="form-control" placeholder="' + item.placeHolder + '" ' + (item.inputText ? 'name="' + item.inputText + '"' : '') + ' ng-model="model.strings[' + scope.currentLine + '].response"> ' +
                        '<label></label>' +
                        '<span class="input-group-btn"> ' +
                        '<button class="btn btn-primary text-normal ink-reaction" type="button" ng-click="onSubmitInput()">Responder <i class="fa fa-send"></i></button> ' +
                        '</span>' +
                        
                        '</div>';
                    var input = angular.element(template);                    
                    scope.cursorElement.before(input);                    
                    $compile(input)(scope);

                    scope.currentElementLine = input;

                    scope.cursorElement.hide();
                    $timeout(function () {
                        scope.currentElementLine.children('input')[0].focus();
                    });                    
                }
                scope.renderSelect = function (item) {

                    var template = '<div class="btn-group typed-select pull-right animate-left" role="group">';
                    angular.forEach(item.options, function (value, key) {
                        template += '<button type="button" class="btn btn-primary" ng-click="onSubmitInput(' + key + ')">'+value.text+'</button>';
                    });
                    template += '</div>';
                    var input = angular.element(template);
                    scope.cursorElement.before(input);
                    $compile(input)(scope);

                    scope.currentElementLine = input;
                    scope.cursorElement.hide();                    
                }
                scope.onSubmitInput = function (value) {
                    var item = scope.model.strings[scope.currentLine];

                    var displayText = item.response;
                    if (item.select) {
                        var selectedOpt = item.options[value];
                        item.response = selectedOpt.value;
                        displayText = selectedOpt.text;
                    }

                    if (item.submitResponse && typeof item.submitResponse == "function") {
                        item.submitResponse(item.response);
                    }

                    //change the input to response text
                    scope.currentElementLine.remove();
                    scope.renderText(displayText, 'text-right');

                    // call next line
                    scope.newLine();
                    scope.nextText(true);
                }
                // initialize all flow
                scope.init();
            }
        };
    }])