﻿lvl999App.controller('loginController', ['$scope', '$http', "$timeout", function ($scope, $http, $timeout) {

    $scope.formShow = false;
    $scope.gameShow = true;
    $scope.formData = {};
    $scope.formData.user = "admin";
    $scope.formData.pass = [];

    $scope.onFormInit = function () {
        $timeout(function () {
            $scope.formShow = true;
            $timeout(function () {
                $g.init(new Phaser.Game($("#level999-game").width(), $("#level999-game").height(), Phaser.CANVAS, 'level999-game'));
            }, 200);
        }, 500);
    }

    $scope.onFormSubmit = function (action) {
        $http({
            method: 'POST',
            url: action,
            cache: false,
            data: JSON.stringify($scope.formData),
            headers: {
                'Content-type': 'application/json'
            }
        })
        .success(function (data) {
            if (data.status === "OK") {
                $scope.formShow = false;
                $scope.gameShow = false;
                $timeout(function () { window.location = data.redirect; }, 500);
            } else {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['error']("Usuário ou senha inválidos.", '');
            }
        });
    };

}]);