﻿lvl999App.controller('mainController', ['$scope', "$timeout", "Home", function ($scope, $timeout, Home) {

    $scope.model = {};

    // actions
    $scope.onInit = function () {
        Home.getProfile(null, function (data) {
            if (data.status === "OK") {
                $scope.model.profile = data.data;
            }            
        });
    }
}]).directive("lvl999Header", function () {
    return {
        scope: {
            profile: "="
        },
        restrict: 'E',
        replace: 'true',
        templateUrl: 'Templates/Home/header.html',
        link: function ($scope, element, attrs) {
            $scope.model = {};
            $scope.toggleControl = true;

            
        }
    }
});