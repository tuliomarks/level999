﻿lvl999App.controller('entranceController', ['$scope', '$http', '$timeout', 'Enterprise', function ($scope, $http, $timeout, Enterprise) {

    $scope.formShow = false;
    $scope.onSubmitResponse = function () {

        var data = {};
        angular.forEach($scope.model.chats, function (entry, idx) {
            if (entry.input) {
                data[entry.input] = entry.response;
            }
        });
        Enterprise.entrance(data, function (data) {
            if (data.status === "OK") {                
                $timeout(function () { window.location = data.redirect; }, 500);
            } else {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['error'](data.message, '');
            }
        });
    }
    $scope.model = {
        names: {
            male: ['Rudolph', 'Jospeh', 'Carroll', 'Jamal', 'Franklyn', 'Erik', 'Lyman', 'Adrian', 'Alphonse', 'Alexis'],
            female: ['Francine', 'Hiroko', 'Robbyn', 'Adriana', 'Cristal', 'Mona', 'Megan', 'Tawanna', 'Shawnee', 'Elyse']
        },
        chats: [
            { message: '<strong>???</strong>:^500 Hey, olá! Meu nome é {person-name}.' },
            { message: '<strong>{person-name}</strong>:^500 Fico muito contente de vê-lo por aqui, seja muito bem vindo ao LudoTask.' },
            { message: '<strong>{person-name}</strong>:^500 Percebo que você quer otimizar o seu tempo e aproveitá-lo para fazer o que realmente importa, você veio ao lugar certo.' },
            { message: '<strong>{person-name}</strong>:^500 Eu vou te ajudar a fazer isso daqui para frente, aliás como posso chamá-lo?' },
            { input: 'name', endlineTimeout: 0, placeHolder: "Escreva seu nome :)" },
            { message: '<strong>{person-name}</strong>:^500 A plataforma do LudoTask é gamificada, e isso quer dizer que você vai se divertir enquanto realiza as suas tarefas.' },
            { message: '<strong>{person-name}</strong>:^500 Para que eu possa te lembrar de ser incrível, preciso saber um email que você olhe frequentemente, pode me dizer qual é?' },
            { input: 'email', endlineTimeout: 0, placeHolder: "Escreva seu email" },
            { message: '<strong>{person-name}</strong>:^500 Ótimo, estou muito empolgado, quero te mostrar como tudo funciona, vamos começar?' },
            {
                select: 'submit',
                options: [
                { text: 'Eh, me parece bom!', value: 'S' },
                { text: 'Estava na hora.', value: '?' },
                ],
                submitResponse: $scope.onSubmitResponse
            },
        ]
    }

    $scope.onInit = function () {

        var array = $scope.model.names.female;
        if (Math.floor(Math.random() * 2)) {
            array = $scope.model.names.male;
        }
        var name = array[Math.floor(Math.random() * array.length)];
        $scope.model.chats = $scope.model.chats.map(function (x) {
            if (x.message)
                x.message = x.message.replace("{person-name}", name);
            return x;
        });
        $timeout(function () {
            $scope.formShow = true;
        }, 500);
    }
}]);