﻿lvl999App.controller('homeController', ['$scope', '$http', "$timeout", 'Tasks', function ($scope, $http, $timeout, Tasks) {

    // task form
    $scope.taskFormShow = false;
    $scope.taskFormCallback = function (result) {
        if (result) {
            $scope.model.tasks.push(result);
        }
        $scope.model.newTaskText = "";
        $scope.setDefaultFocus();
    }

    // properties 
    $scope.model = {
        tasks: [],
        newTaskText: ""
    };
    $scope.taskBarShow = false;
    $scope.gameShow = false;

    // actions
    $scope.onInit = function () {
        $scope.model.count = 0;

        $timeout(function () {
            $scope.taskBarShow = true;
            $scope.gameShow = true;
        }, 200);

        $timeout(function () {           
            $g.init(new Phaser.Game($("#level999-game").width(), $("#level999-game").height(), Phaser.CANVAS, 'level999-game'));
        }, 500);

        // set focus
        $scope.setDefaultFocus();

        Tasks.list(function (data) {
            $scope.model.tasks = data;
        });
    }
    $scope.setDefaultFocus = function() {
        $timeout(function () {
            angular.element('#content').children().children()[0].focus();
        });
    }
    $scope.onClickAddTask = function () {
        $scope.taskFormShow = true;
    }
    $scope.onClickDoneTask = function (tska001) {
        Tasks.finish({ id: tska001.Tska001Id }, function (data) {
            if (data.status === "OK") {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['success']("Tarefa concluída!!!", '');
                tska001.Tska001Datefinished = data.dateFinished;
            } else {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['warning']("Não foi possível concluír a tarefa.", '');
            }

        });
    }
    $scope.onClickRemoveTask = function ($event, tska001) {
        Tasks.delete({ id: tska001.Tska001Id }, function (data) {
            if (data.status === "OK") {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['success']("Tarefa removida com sucesso.", '');
                var index = $scope.model.tasks.indexOf(tska001);
                $scope.model.tasks.splice(index, 1);
            } else {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['warning']("Não foi possível remover a tarefa.",'');
            }

        });
    }
    $scope.onKeyPress = function (event) {
        if (event.keyCode !== 13 && !$scope.taskFormShow) {
            //console.log(event.keyCode);
            $scope.model.newTaskText = $scope.model.newTaskText + String.fromCharCode(event.keyCode);
            $timeout(function () {
                if (!$scope.taskFormShow) {
                    $scope.taskFormShow = true;
                }
            }, 100);
        }
    }
}]);

lvl999App.controller('matrixController', ['$scope', '$http', "$timeout", 'Tasks', function ($scope, $http, $timeout, Tasks) {

    $scope.matrixShow = false;
    $scope.model = { habits: [], dailys: [], tasks: [] };
    $scope.taskForm = { data: {} };

    $scope.onInit = function () {
        $timeout(function () {
            $scope.matrixShow = true;
        }, 200);
    }

    $scope.taskForm.onFormSubmit = function () {
        Tasks.insert($scope.taskForm.data, function (data) {
            if (data.status === "OK") {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['success']("Tarefa inserida com sucesso.", '');
                $scope.model.tasks.push(data.data);
            } else {
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": 330,
                    "hideDuration": 330,
                    "timeOut": 1500,
                    "extendedTimeOut": 1000,
                    "showEasing": "swing",
                    "hideEasing": "swing",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                toastr['error']("Não foi possível inserir a tarefa.", '');
            }
        });
    }
    $scope.initialize = function () {
        Tasks.list(function (data) {
            $scope.model.tasks = data;
        });
    };

    $scope.initialize();
}]);

lvl999App.directive("lvl999TasksForm", ['$timeout', 'Tasks', function ($timeout, Tasks) {
    return {
        scope: {
            show: '=',
            callback: '=',
            text: '='
        },
        restrict: 'E',
        replace: 'true',
        templateUrl: 'Templates/tasks/form.html',
        link: function ($scope, element, attrs) {
            $scope.model = {};
            $scope.toggleControl = true;

            $scope.toggleModal = function (visible) {
                if (visible && $scope.toggleControl) {
                    // clear the model to new task
                    $scope.model = {};
                    $scope.model.title = $scope.text;
                    // set focus
                    $timeout(function () {
                        element[0].querySelector("input").focus();
                    });

                    $scope.toggleControl = false;
                } else if (!visible) {
                    $scope.toggleControl = true;
                }
                return visible;
            }
            $scope.onFormClose = function () {
                if ($scope.callback != undefined && typeof ($scope.callback) == "function") {
                    $scope.callback();
                    $scope.show = false;
                }
            }
            $scope.onFormSubmit = function () {
                Tasks.insert($scope.model, function (data) {
                    if (data.status === "OK") {
                        toastr.options = {
                            "positionClass": "toast-top-right",
                            "showDuration": 330,
                            "hideDuration": 330,
                            "timeOut": 1500,
                            "extendedTimeOut": 1000,
                            "showEasing": "swing",
                            "hideEasing": "swing",
                            "showMethod": "slideDown",
                            "hideMethod": "slideUp"
                        }
                        toastr['success']("Tarefa inserida com sucesso.", '');
                        if ($scope.callback != undefined && typeof ($scope.callback) == "function") {
                            $scope.callback(data.model);
                            $scope.show = false;
                        }
                    } else {
                        toastr.options = {
                            "positionClass": "toast-top-right",
                            "showDuration": 330,
                            "hideDuration": 330,
                            "timeOut": 2500,
                            "extendedTimeOut": 1000,
                            "showEasing": "swing",
                            "hideEasing": "swing",
                            "showMethod": "slideDown",
                            "hideMethod": "slideUp"
                        }
                        toastr['warning']("Não foi possível inserir a tarefa.", data.message);
                    }
                });
            }
        }
    }
}]);