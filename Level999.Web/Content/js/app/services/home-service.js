﻿lvl999Services.factory('Home', ['$http', function ($http) {
    return {              
        getProfile: function (data, callback) {
            $http({
                method: 'GET',
                url: 'Home/GetProfile',
                cache: false,
                data: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })
          .success(function (data) {
              callback(data);
          });
        }
    };
}]);