﻿lvl999Services.factory('Enterprise', ['$http', function ($http) {
    return {              
        entrance: function (data, callback) {
            $http({
                method: 'POST',
                url: 'Enterprise/Entrance',
                cache: false,
                data: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })
          .success(function (data) {
              callback(data);
          });
        }
    };
}]);