﻿lvl999Services.factory('Tasks', ['$http', function ($http) {
    return {
        list: function (callback) {
            $http.get('tasks/list').success(function (data) {
                if (data.status === 'OK') {
                    callback(data.list);
                } else {
                    callback();
                }
            });
        },
        insert: function (data, callback) {
            $http({
                method: 'POST',
                url: 'tasks/insert',
                cache: false,
                data: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })
          .success(function (data) {
              callback(data);
          });
        },
        delete: function (data, callback) {
            $http({
                method: 'POST',
                url: 'tasks/delete',
                cache: false,
                data: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })
          .success(function (data) {
              callback(data);
          });
        },
        finish: function (data, callback) {
        $http({
            method: 'POST',
            url: 'tasks/finish',
            cache: false,
            data: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json'
            }
        })
      .success(function (data) {
          callback(data);
      });
    }
    };
}]);