﻿var lvl999Services = angular.module('lvl999Services', []);
var lvl999App = angular.module('lvl999App', ['ngRoute', 'ngAnimate', 'lvl999Services', 'angularTyped'])
.config(function ($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/', {
            templateUrl: 'Templates/Home/Index.html',
            controller: 'homeController'
        })
        .when('/matrix', {
            templateUrl: 'Templates/Home/Matrix.html',
            controller: 'matrixController'
        })
        // route for the about page
        .when('/inventory', {
            templateUrl: 'Templates/Inventory/Index.html',
            controller: 'inventoryController'
        })
        // route for the contact page
        .when('/market', {
            templateUrl: 'Templates/Market/Index.html',
            controller: 'marketController'
        });
});

lvl999App.directive("inkReaction", function () {
    return {
        restrict: 'C',
        link: function ($scope, element, attrs) {
            var getBackground = function (item) {
                // Is current element's background color set?
                var color = item.css("background-color");
                var alpha = parseFloat(color.split(',')[3], 10);

                if ((isNaN(alpha) || alpha > 0.8) && color !== 'transparent') {
                    // if so then return that color if it isn't transparent
                    return color;
                }

                // if not: are you at the body element?
                if (item.is("body")) {
                    // return known 'false' value
                    return false;
                } else {
                    // call getBackground with parent item
                    return getBackground(item.parent());
                }
            };

            var getLuma = function (color) {
                var rgba = color.substring(4, color.length - 1).split(',');
                var r = rgba[0];
                var g = rgba[1];
                var b = rgba[2];
                var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
                return luma;
            };

            element.on('click', function (e) {
                var color = getBackground($(this));
                var inverse = (getLuma(color) > 183) ? ' inverse' : '';

                var ink = $('<div class="ink' + inverse + '"></div>');
                var btnOffset = element.offset();
                var xPos = e.pageX - btnOffset.left;
                var yPos = e.pageY - btnOffset.top;

                ink.css({
                    top: yPos,
                    left: xPos
                }).appendTo(element);

                window.setTimeout(function () {
                    ink.remove();
                }, 1500);
            });
        }
    }
});