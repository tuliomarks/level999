﻿using System;
using System.Data;
using System.Data.Common;

namespace Level999.Framework.Data.Extensions
{
    public static class DataReaderExtension
    {
        #region Constructors
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Events
        #endregion

        #region Methods
        public static bool Fetch(this DbDataReader idr)
        {
            return idr != null && idr.HasRows && idr.Read();
        }

        public static object GetValue(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetValue(idr.GetOrdinal(param)) : null;
        }

        public static string GetString(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetString(idr.GetOrdinal(param)) : string.Empty;
        }

        public static short GetShort(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetInt16(idr.GetOrdinal(param)) : short.MinValue;
        }

        public static int GetInt(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetInt32(idr.GetOrdinal(param)) : int.MinValue;
        }

        public static int? GetNullableInt(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? (int?)idr.GetInt32(idr.GetOrdinal(param)) : null;
        }

        public static long GetLong(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetInt64(idr.GetOrdinal(param)) : long.MinValue;
        }

        public static long? GetNullableLong(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? (long?)idr.GetInt64(idr.GetOrdinal(param)) : null;
        }

        public static double GetDouble(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetDouble(idr.GetOrdinal(param)) : double.MinValue;
        }

        public static double? GetNullableDouble(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? (double?)idr.GetDouble(idr.GetOrdinal(param)) : null;
        }

        public static decimal GetDecimal(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetDecimal(idr.GetOrdinal(param)) : decimal.MinValue;
        }

        public static decimal? GetNullableDecimal(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? (decimal?)idr.GetDecimal(idr.GetOrdinal(param)) : null;
        }

        public static bool GetBool(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? Convert.ToBoolean(idr.GetOrdinal(param)) : false;
        }

        public static DateTime GetDateTime(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? idr.GetDateTime(idr.GetOrdinal(param)) : DateTime.MinValue;
        }

        public static DateTime? GetNullableDateTime(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? (DateTime?)idr.GetDateTime(idr.GetOrdinal(param)) : null;
        }

        public static byte[] GetBlob(this IDataReader idr, string param)
        {
            return (!idr.IsDBNull(idr.GetOrdinal(param))) ? (byte[])idr[param] : new byte[0];
        }
        #endregion
    }
}
