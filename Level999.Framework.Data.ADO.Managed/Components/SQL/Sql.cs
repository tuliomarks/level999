﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Level999.Framework.Data.Connection;
using Level999.Framework.Data.Converters;
using Level999.Framework.Data.Extensions;
using MySql.Data.MySqlClient;

namespace Level999.Framework.Data.Components.SQL
{
    public class Sql : ISql
    {
        #region Constructors
        public Sql()
        {
            DefaultConstructor();
            _connection = new Connection.MySqlConnection();
        }

        ~Sql()
        {
            _connection.Disconnect();
        }

        public Sql(string command)
            : this()
        {
            _command = command;
        }

        public Sql(string host, int port, string serviceName, string userId, string password)
        {
            DefaultConstructor();
            _connection = new Connection.MySqlConnection(host, port, serviceName, userId, password);
        }

        public Sql(string host, int port, string serviceName, string userId, string password, string command)
            : this(host, port, serviceName, userId, password)
        {
            _command = command;
        }

        public Sql(string host, int port, string serviceName, string userId, string password,
                   int minPoolSize, int maxPoolSize, int incPoolSize, int decPoolSize)
        {
            DefaultConstructor();
            _connection = new Connection.MySqlConnection(host, port, serviceName, userId, password, minPoolSize, maxPoolSize, incPoolSize, decPoolSize);
        }

        public Sql(string host, int port, string serviceName, string userId, string password,
                   int minPoolSize, int maxPoolSize, int incPoolSize, int decPoolSize, string command)
            : this(host, port, serviceName, userId, password, minPoolSize, maxPoolSize, incPoolSize, decPoolSize)
        {
            _command = command;
        }
        #endregion

        #region Fields
        private readonly Connection.MySqlConnection _connection;
        private string _command;
        private IList<Bind> _bindings;
        private Regex _regexBinds;
        private Bind _return;
        private int _paginationStart;
        private int _paginationQuantity;
        private bool _isPaginated;
        private Stopwatch _watch;
        #endregion

        #region Properties
        public static bool? IsDebug { get; set; }
        #endregion

        #region Events
        #endregion

        #region Methods
        private void DefaultConstructor()
        {
            _command = string.Empty;
            _bindings = new List<Bind>();
            _return = null;
            _paginationStart = 0;
            _paginationQuantity = 0;
            _isPaginated = false;
            _regexBinds = new Regex(@":\s*([\dA-Za-z_]*)(?=([^'\\]*(\\.|'([^'\\]*\\.)*[^'\\]*'))*[^']*$)");

            try
            {
                if (!IsDebug.HasValue)
                    IsDebug = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SqlDebug"]);
            }
            catch (FormatException)
            {
                // Não deve ocorrer erro caso o parametro nao esteja declarado corretamente
            }
        }

        // Connection
        public void Disconnect()
        {
            _connection.Disconnect();
        }

        // Transaction
        public void BeginTransaction()
        {
            _connection.BeginTransaction();
        }

        public void EndTransaction(bool commit)
        {
            _connection.EndTransaction(commit);
        }

        public void Commit()
        {
            _connection.Commit();
        }

        public void Rollback()
        {
            _connection.Rollback();
        }

        // Command Definition
        private Sql AddBind(Bind bind)
        {
            //if (bind.Type == MySqlDbType.VarChar && bind.Value != null && (bind.Value.ToString()).Length > 4000)
            //{
            //    bind.Type = MySqlDbType.Clob;
            //}

            _bindings.Add(bind);

            return this;
        }

        private Sql AddReturn(Bind bind)
        {
            if (_return != null)
            {
                throw new Exception("Não é possível haver dois retornos simultaneamente.");
            }

            _return = bind;

            return this;
        }

        public ISql AddBind(object value)
        {
            return AddBind(new Bind("", value, TypeConverter.ToDataBaseType(value), ParameterDirection.Input));
        }

        public ISql AddBind<T>(object value) where T : Type, new()
        {
            var type = new T();
            return AddBind(new Bind("", value, TypeConverter.ToDataBaseType(type), ParameterDirection.Input));
        }

        public ISql AddBind(string name, object value)
        {
            return AddBind(new Bind(name, value, TypeConverter.ToDataBaseType(value), ParameterDirection.Input));
        }

        public ISql AddBind(string name, object value, Type type)
        {
            return AddBind(new Bind(name, value, TypeConverter.ToDataBaseType(type), ParameterDirection.Input));
        }

        public ISql AddBind(string name, object value, ParameterDirection direction)
        {
            return AddBind(new Bind(name, value, TypeConverter.ToDataBaseType(value), direction));
        }

        public ISql AddBind(string name, object value, Type type, ParameterDirection direction)
        {
            return AddBind(new Bind(name, value, TypeConverter.ToDataBaseType(type), direction));
        }

        public ISql AddReturn(Type type)
        {
            return AddReturn(new Bind("return", TypeConverter.ToDataBaseType(type), ParameterDirection.ReturnValue));
        }

        public ISql AggregateCommand(string command)
        {
            _command += string.Format(" {0}", command);

            return this;
        }

        public ISql AggregateCommand(string name, ISql sql)
        {
            var sqlnew = sql as Sql;
            if (sqlnew == null)
            {
                return this;
            }

            _command = _command.Replace(string.Format(":{0}", name), sqlnew._command);
            foreach (var binding in sqlnew._bindings)
            {
                _bindings.Add(binding);
            }

            return this;
        }

        public ISql SetCommand(string command)
        {
            _command = command;

            return this;
        }

        public ISql SetPagination(int start, int quantity)
        {
            if (quantity == 0 && start == 0)
            {
                return this;
            }

            if (quantity <= 0)
            {
                throw new Exception("A quantidade deve ser maior que 0 (zero).");
            }

            if (start <= 0)
            {
                start = 1;
            }

            if (quantity > 0)
            {
                _paginationStart = start;
                _paginationQuantity = quantity;
                _isPaginated = true;
            }

            return this;
        }

        public ISql ClearParameters()
        {
            _connection.ClearParameters();
            _command = string.Empty;
            _bindings.Clear();
            _return = null;
            _paginationStart = 0;
            _paginationQuantity = 0;
            _isPaginated = false;

            return this;
        }

        // Command Execution
        public void PrepareToExecute()
        {
            try
            {
                _connection.ClearParameters();

                if (_return != null)
                {                    
                    _connection.AddTypedReturnParameter(_return);
                }

                ProcessBinds();
            }
            catch (MySqlException e)
            {
                ThrowException(e);
            }
        }

        public void ProcessBinds()
        {
            var commandMatchs = _regexBinds.Matches(_command);

            if (commandMatchs.Count == _bindings.Count || commandMatchs.Count == 0)
            {
                foreach (var binding in _bindings)
                {
                    _connection.AddParameter(binding);
                }
            }
            else
            {
                // dentro de cada match o group contem o bind
                // Ex: select :num from dual
                // match= select :num 
                // group1= num
                foreach (var bind in commandMatchs.Cast<Match>().Select(regex => _bindings.FirstOrDefault(b => regex.Groups[1].Value.ToString().Replace(" ", "").Equals(b.Name))).Where(bind => bind != null))
                {
                    _connection.AddParameter(bind);
                }
            }
        }

        public int ExecuteCount()
        {
            try
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch = Stopwatch.StartNew();
                }

                PrepareToExecute();

                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Preparing Query: {0}", _watch.Elapsed));
                    _watch.Start();
                }

                return _connection.ExecuteScalar(string.Format("SELECT COUNT(1) FROM ({0})", _command));

            }
            catch (MySqlException e)
            {
                throw ThrowException(e);
            }
            finally
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Executing Query: {0}", _watch.Elapsed));
                }
            }
        }

        public DateTime ExecuteSysdate()
        {
            try
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch = Stopwatch.StartNew();
                }
                PrepareToExecute();

                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Preparing Query: {0}", _watch.Elapsed));
                    _watch.Start();
                }

                var dr = (DbDataReader)_connection.ExecuteQuery("SELECT SYSDATE FROM DUAL");
                return dr.Fetch() ? dr.GetDateTime("SYSDATE") : DateTime.MinValue;
            }
            catch (MySqlException e)
            {
                throw ThrowException(e);
            }
            finally
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Executing Query: {0}", _watch.Elapsed));
                }
            }
        }

        public DbDataReader ExecuteQuery()
        {
            try
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch = Stopwatch.StartNew();
                }

                PrepareToExecute();
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Preparing Query: {0}", _watch.Elapsed));
                    _watch.Start();
                }
                if (_isPaginated)
                {
                    _connection.AddParameter(new Bind("ULTIMO", _paginationStart * _paginationQuantity, MySqlDbType.Int32, ParameterDirection.Input));
                    _connection.AddParameter(new Bind("PRIMEIRO", (_paginationStart - 1) * _paginationQuantity, MySqlDbType.Int32, ParameterDirection.Input));

                    return (DbDataReader)_connection.ExecuteQuery(string.Format("SELECT * " +
                                                                                  "FROM (SELECT CONSULTA.*, ROWNUM RNUM " +
                                                                                          "FROM ({0}) CONSULTA " +
                                                                                         "WHERE ROWNUM <= :ULTIMO) " +
                                                                                 "WHERE RNUM > :PRIMEIRO",
                                                                                 _command));
                }

                return (DbDataReader)_connection.ExecuteQuery(_command);
            }
            catch (MySqlException e)
            {
                throw ThrowException(e);
            }
            finally
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Executing Query: {0}", _watch.Elapsed));
                }
            }
        }

        public int ExecuteNonQuery()
        {
            try
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch = Stopwatch.StartNew();
                }

                PrepareToExecute();

                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Preparing Query: {0}", _watch.Elapsed));
                    _watch.Start();
                }

                return _connection.ExecuteNonQuery(_command);
            }
            catch (MySqlException e)
            {
                throw ThrowException(e);
            }
            finally
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Executing Query: {0}", _watch.Elapsed));
                }
            }
        }

        public DbDataReader ExecuteFunction()
        {
            try
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch = Stopwatch.StartNew();
                }

                PrepareToExecute();

                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Preparing Query: {0}", _watch.Elapsed));
                    _watch.Start();
                }

                return (DbDataReader)_connection.ExecuteFunction(_command);
            }
            catch (MySqlException e)
            {
                throw ThrowException(e);
            }
            finally
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Executing Query: {0}", _watch.Elapsed));
                }
            }
        }

        public void ExecuteProcedure()
        {

            try
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch = Stopwatch.StartNew();
                }

                PrepareToExecute();

                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Preparing Query: {0}", _watch.Elapsed));
                    _watch.Start();
                }

                _connection.ExecuteProcedure(_command);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (IsDebug.HasValue && IsDebug.Value)
                {
                    _watch.Stop();
                    Trace.WriteLine(string.Format("Time Executing Query: {0}", _watch.Elapsed));
                }
            }
        }

        // Command Return
        private object GetValue(string name = "return")
        {
            return _connection.GetValue(name);
        }

        public string GetString(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? string.Empty : value.ToString();
        }

        public int GetInt(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? int.MinValue : Convert.ToInt32(value.ToString());
        }

        public int? GetNullableInt(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? null : (int?)Convert.ToInt32(value.ToString());
        }

        public long GetLong(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? long.MinValue : Convert.ToInt64(value.ToString());
        }

        public long? GetNullableLong(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? null : (long?)Convert.ToInt64(value.ToString());
        }

        public double GetDouble(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? double.MinValue : Convert.ToDouble(value.ToString(), CultureInfo.InvariantCulture);
        }

        public double? GetNullableDouble(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? null : (double?)Convert.ToDouble(value.ToString());
        }

        public decimal GetDecimal(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? decimal.MinValue : Convert.ToDecimal(value.ToString(), CultureInfo.InvariantCulture);
        }

        public decimal? GetNullableDecimal(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? null : (decimal?)Convert.ToDecimal(value.ToString());
        }

        public bool GetBool(string name = "return")
        {
            var value = GetValue(name);
            return (value != null) && Convert.ToBoolean(value);
        }

        public DateTime GetDateTime(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? DateTime.MinValue : Convert.ToDateTime(value);
        }

        public DateTime? GetNullableDateTime(string name = "return")
        {
            var value = GetValue(name);
            return (value == null) ? null : (DateTime?)Convert.ToDateTime(value.ToString());
        }

        //Exceptions Treatments
        public virtual Exception ThrowException(MySqlException e)
        {
            //Método que será sobrescrito
            throw new NotImplementedException("O método ThrowException do SQL não foi implementado!", e);
        }

        public void Dispose()
        {
            _connection.Disconnect();
        }
        #endregion
    }
}