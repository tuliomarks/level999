﻿using System.Data;
using MySql.Data.MySqlClient;

namespace Level999.Framework.Data.Components.SQL
{
    public class Bind
    {
        #region Constructors
        public Bind()
        {
            Name = string.Empty;
            Value = null;
            Type = MySqlDbType.VarChar;
            Direction = ParameterDirection.Input;
        }

        public Bind(string name, MySqlDbType type, ParameterDirection direction)
        {
            Name = name;
            Value = null;
            Type = type;
            Direction = direction;
        }

        public Bind(string name, object value, MySqlDbType type, ParameterDirection direction)
        {
            Name = name;
            Value = value;
            Type = type;
            Direction = direction;
        }
        #endregion

        #region Fields
        #endregion

        #region Properties
        public string Name { get; set; }
        public object Value { get; set; }
        public MySqlDbType Type { get; set; }
        public ParameterDirection Direction { get; set; }
        #endregion

        #region Events
        #endregion

        #region Methods
        #endregion
    }
}
