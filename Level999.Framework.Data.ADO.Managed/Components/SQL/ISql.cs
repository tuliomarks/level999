﻿using System;
using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Level999.Framework.Data.Components.SQL
{
    public interface ISql : IDisposable
    {
        void Disconnect();
        void BeginTransaction();
        void EndTransaction(bool commit);
        void Commit();
        void Rollback();
        ISql AddBind(object value);
        ISql AddBind<T>(object value) where T: Type, new();
        ISql AddBind(string name, object value);
        ISql AddBind(string name, object value, Type type);
        ISql AddBind(string name, object value, ParameterDirection direction);
        ISql AddBind(string name, object value, Type type, ParameterDirection direction);
        ISql AddReturn(Type type);
        ISql AggregateCommand(string command);
        ISql AggregateCommand(string name, ISql sql);
        ISql SetCommand(string command);
        ISql SetPagination(int start, int quantity);
        ISql ClearParameters();
        void PrepareToExecute();
        void ProcessBinds();
        int ExecuteCount();
        DateTime ExecuteSysdate();
        DbDataReader ExecuteQuery();
        int ExecuteNonQuery();
        DbDataReader ExecuteFunction();
        void ExecuteProcedure();
        string GetString(string name = "return");
        int GetInt(string name = "return");
        int? GetNullableInt(string name = "return");
        long GetLong(string name = "return");
        long? GetNullableLong(string name = "return");
        double GetDouble(string name = "return");
        double? GetNullableDouble(string name = "return");
        decimal GetDecimal(string name = "return");
        decimal? GetNullableDecimal(string name = "return");
        bool GetBool(string name = "return");
        DateTime GetDateTime(string name = "return");
        DateTime? GetNullableDateTime(string name = "return");
        Exception ThrowException(MySqlException e);
        new void Dispose();
    }
}