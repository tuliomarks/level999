﻿namespace Level999.Framework.Data.Components.SQL
{
    public class ConnectionString
    {
        #region Constructors
        public ConnectionString(string host, int port, string serviceName, string userId, string password)
        {
            Host = host;
            Port = port;
            ServiceName = serviceName;
            UserId = userId;
            Password = password;

            MinimunPoolSize = 5;
            MaximunPoolSize = 20;
            IncrementPoolSize = 5;
            DecrementPoolSize = 5;
        }

        public ConnectionString(string host, int port, string serviceName, string userId, string password, 
                                      int minPoolSize, int maxPoolSize, int incPoolSize, int decPoolSize)
            : this(host, port, serviceName, userId, password)
        {
            MinimunPoolSize = minPoolSize;
            MaximunPoolSize = maxPoolSize;
            IncrementPoolSize = incPoolSize;
            DecrementPoolSize = decPoolSize;
        }
        #endregion

        #region Fields
        #endregion

        #region Properties
        public string Host { get; set; }
        public int Port { get; set; }
        public string ServiceName { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public int MinimunPoolSize { get; set; }
        public int MaximunPoolSize { get; set; }
        public int IncrementPoolSize { get; set; }
        public int DecrementPoolSize { get; set; }
        #endregion

        #region Events
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format(
                    "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1}))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={2}))); " +
                    "User Id={3}; " +
                    "Password={4}; " +
                    "Min Pool Size={5}; " +
                    "Max Pool Size={6}; " +
                    "Incr Pool Size={7}; " +
                    "Decr Pool Size={8}",
                    Host,
                    Port,
                    ServiceName,
                    UserId,
                    Password,
                    MinimunPoolSize,
                    MaximunPoolSize,
                    IncrementPoolSize,
                    DecrementPoolSize);
        }
        #endregion
    }
}
