﻿using System;
using MySql.Data.MySqlClient;

namespace Level999.Framework.Data.Components
{
    public class Ora20000Exception : Exception
    {
        public Ora20000Exception(MySqlException exception)
            : base(GetMessage(exception), exception)
        {
        }

        protected static string GetMessage(MySqlException exception)
        {
            var mensagem = exception.Message;
            if (exception.Number == 20000)
            {
                mensagem = mensagem.Replace("ORA-20000:", string.Empty).Trim();
                if (mensagem.IndexOf("ORA") > 0)
                    mensagem = mensagem.Substring(0, mensagem.IndexOf("ORA"));
                if (mensagem.IndexOf("Origem erro:") > 0)
                    mensagem = mensagem.Substring(0, mensagem.IndexOf("Origem erro:", System.StringComparison.Ordinal));
            }
            return mensagem;
        }
    }
}
