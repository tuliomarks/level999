﻿using System;
using MySql.Data.MySqlClient;

namespace Level999.Framework.Data.Converters
{
    public class TypeConverter
    {
        #region Constructors
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Events
        #endregion

        #region Methods
        public static MySqlDbType ToDataBaseType(Type type)
        {
            if (type == typeof (short)) return MySqlDbType.Int16;
            if (type == typeof(int)) return MySqlDbType.Int32;
            if (type == typeof(long)) return MySqlDbType.Int64;
            if (type == typeof(float)) return MySqlDbType.Float;
            if (type == typeof(double)) return MySqlDbType.Double;
            if (type == typeof(decimal)) return MySqlDbType.Decimal;
            if (type == typeof(string)) return MySqlDbType.VarChar;
            if (type == typeof(DateTime)) return MySqlDbType.DateTime;
            if (type == typeof(byte)) return MySqlDbType.Byte;
            if (type == typeof(byte[])) return MySqlDbType.Blob;
            if (type == typeof(DBNull)) return MySqlDbType.VarChar;
            if (type == typeof(bool)) return MySqlDbType.Bit;
            return MySqlDbType.VarChar;
        }

        public static MySqlDbType ToDataBaseType(object type)
        {
            if (type is short) return MySqlDbType.Int16;
            if (type is int) return MySqlDbType.Int32;
            if (type is long) return MySqlDbType.Int64;
            if (type is float) return MySqlDbType.Float;
            if (type is double) return MySqlDbType.Double;
            if (type is decimal) return MySqlDbType.Decimal;
            if (type is string) return MySqlDbType.VarChar;
            if (type is DateTime) return MySqlDbType.DateTime;
            if (type is byte) return MySqlDbType.Byte;
            if (type is byte[]) return MySqlDbType.Blob;
            if (type is DBNull) return MySqlDbType.VarChar;
            if (type is bool) return MySqlDbType.Bit;
            return MySqlDbType.VarChar;
        }
        #endregion
    }
}
