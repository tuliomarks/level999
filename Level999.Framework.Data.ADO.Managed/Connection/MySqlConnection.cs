﻿using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using Level999.Framework.Data.Components.SQL;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace Level999.Framework.Data.Connection
{
    public class MySqlConnection
    {
        #region Constructors
        public MySqlConnection()
        {            
            DisconnectAtEndOfExecution = false;
            GetConfigParams();
            GetDatabase();
        }

        public MySqlConnection(string host, int port, string serviceName, string userId, string password)
        {
            DisconnectAtEndOfExecution = false;
            GetConfigParams();
            GetDatabase(host, port, serviceName, userId, password);
        }

        public MySqlConnection(string host, int port, string serviceName, string userId, string password,
                                int minPoolSize, int maxPoolSize, int incPoolSize, int decPoolSize)
        {
            DisconnectAtEndOfExecution = false;
            GetConfigParams();
            GetDatabase(host, port, serviceName, userId, password, minPoolSize, maxPoolSize, incPoolSize, decPoolSize);
        }
        #endregion

        #region Attributes
        private string _connectionString;
        private MySql.Data.MySqlClient.MySqlConnection _connection;
        private MySqlTransaction _transaction;
        private MySqlCommand _command;
        #endregion

        #region Properties
        public bool DisconnectAtEndOfExecution { get; set; }

        public static bool? IsDebug { get; set; }
        #endregion

        #region Events
        #endregion

        #region Methods

        private void GetConfigParams()
        {
            try
            {
                if (!IsDebug.HasValue)
                    IsDebug = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["OracleConnectionDebug"]);
            }
            catch (FormatException)
            {
                // Não deve ocorrer erro caso o parametro nao esteja declarado corretamente
            }
        }

        // Connection
        public void GetDatabase()
        {
            try
            {
                var connString = ConfigurationManager.ConnectionStrings["dbStr"];
                if (connString == null)
                {
                    throw new ArgumentException("Connection String named dbStr");
                }

                _connectionString = connString.ConnectionString;
                _connection = new MySql.Data.MySqlClient.MySqlConnection();
                _command = new MySqlCommand();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void GetDatabase(string host, int port, string serviceName, string userId, string password)
        {
            try
            {
                var connectionString = new ConnectionString(host, port, serviceName, userId, password);
                _connectionString = connectionString.ToString();
                _connection = new MySql.Data.MySqlClient.MySqlConnection();
                _command = new MySqlCommand();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void GetDatabase(string host, int port, string serviceName, string userId, string password,
                                int minPoolSize, int maxPoolSize, int incPoolSize, int decPoolSize)
        {
            try
            {
                var connectionString = new ConnectionString(host, port, serviceName, userId, password, minPoolSize, maxPoolSize, incPoolSize, decPoolSize);
                _connectionString = connectionString.ToString();
                _connection = new MySql.Data.MySqlClient.MySqlConnection();
                _command = new MySqlCommand();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Connect()
        {
            try
            {
                if (_connection.State != ConnectionState.Open)
                {
                    _connection = new MySql.Data.MySqlClient.MySqlConnection(_connectionString);
                    _connection.Open();
                    _command.Connection = _connection;
                }
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        public void Disconnect()
        {
            _connection.Close();
        }

        // Transaction
        public void BeginTransaction()
        {
            Connect();

            if (_transaction != null)
            {
                EndTransaction();
            }

            _transaction = _connection.BeginTransaction();
        }

        public void EndTransaction(bool commit = true)
        {
            if (_connection.State != ConnectionState.Open)
            {
                return;
            }

            if (_transaction != null)
            {
                if (commit)
                {
                    Commit();
                }
                else
                {
                    Rollback();
                }

                _transaction.Dispose();
                _transaction = null;
            }
        }

        public void Commit()
        {
            if (_connection.State != ConnectionState.Open)
            {
                return;
            }

            if (_transaction != null)
            {
                _transaction.Commit();
            }
        }

        public void Rollback()
        {
            if (_connection.State != ConnectionState.Open)
            {
                return;
            }

            if (_transaction != null)
            {
                _transaction.Rollback();
            }
        }

        // Command Definition
        public void AddParameter(Bind bind)
        {
            var param = new MySqlParameter
                            {
                                ParameterName = bind.Name ?? string.Empty,
                                Value = bind.Value ?? DBNull.Value,
                                MySqlDbType = bind.Type,
                                Direction = bind.Direction
                            };

            if (bind.Type == MySqlDbType.VarChar)
            {
                param.Size = 4000;
            }

            _command.Parameters.Add(param);
        }

        public void AddReturnParameter()
        {
            var param = new MySqlParameter
            {
                                ParameterName = "return",
                                Direction = ParameterDirection.ReturnValue
                            };

            _command.Parameters.Add(param);
        }

        public void AddTypedReturnParameter(Bind bind)
        {
            var param = new MySqlParameter
            {
                                ParameterName = bind.Name,
                                MySqlDbType = bind.Type,
                                Direction = ParameterDirection.ReturnValue
                            };

            if (bind.Type == MySqlDbType.VarChar)
            {
                param.Size = 4000;
            }

            _command.Parameters.Add(param);
        }

        public void ClearParameters()
        {
            _command.Parameters.Clear();
        }

        // Command Execution
        public int ExecuteScalar(string cmd)
        {
            try
            {
                Connect();

                _command.CommandText = cmd;

                if (_transaction != null)
                {
                    _command.Transaction = _transaction;
                }

                DoTrace();

                return int.Parse(_command.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (DisconnectAtEndOfExecution)
                {
                    Disconnect();
                }
            }
        }

        public IDataReader ExecuteQuery(string cmd)
        {
            try
            {
                Connect();

                _command.CommandText = cmd;
                _command.CommandType = CommandType.Text;

                if (_transaction != null)
                {
                    _command.Transaction = _transaction;
                }               

                DoTrace();

                var reader = _command.ExecuteReader();

                // Faz com que retorne apenas 500 registros pelo fetch size
                //reader.FetchSize = 500 * _command.RowSize;

                return reader;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (DisconnectAtEndOfExecution)
                {
                    Disconnect();
                }
            }
        }

        public int ExecuteNonQuery(string cmd)
        {
            try
            {
                Connect();

                _command.CommandText = cmd;
                _command.CommandType = CommandType.Text;

                if (_transaction != null)
                {
                    _command.Transaction = _transaction;
                }

                DoTrace();

                return _command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (DisconnectAtEndOfExecution)
                {
                    Disconnect();
                }
            }
        }

        public IDataReader ExecuteFunction(string cmd)
        {
            try
            {
                Connect();

                _command.CommandText = cmd;
                _command.CommandType = CommandType.StoredProcedure;

                if (_transaction != null)
                {
                    _command.Transaction = _transaction;
                }

                DoTrace();

                return _command.ExecuteReader();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (DisconnectAtEndOfExecution)
                {
                    Disconnect();
                }
            }
        }

        public void ExecuteProcedure(string cmd)
        {
            try
            {
                Connect();

                _command.CommandText = cmd;
                _command.CommandType = CommandType.StoredProcedure;

                if (_transaction != null)
                {
                    _command.Transaction = _transaction;
                }

                DoTrace();

                _command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (DisconnectAtEndOfExecution)
                {
                    Disconnect();
                }
            }
        }

        // Command Return
        public object GetValue(string name)
        {
            try
            {
                var param = _command.Parameters[name];

                //if (param.Status == OracleParameterStatus.NullFetched)
                //{
                //    return null;
                //}

                if (param.Value is MySqlDateTime)
                {
                    // faz isso para retonar apenas o tipo primitivo do .Net DateTime
                    return ((MySqlDateTime)param.Value).Value;
                }

                //var clob = param.Value as MySqlo;
                //return clob != null ? clob.Value : param.Value;

                return param.Value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void DoTrace()
        {
            if (IsDebug.HasValue && IsDebug.Value)
            {
                // Faz esse Trace para listar no Output do Visual Studio
                Trace.WriteLine($"Exec Query: {_command.CommandText}");
                foreach (MySqlParameter param in _command.Parameters)
                {
                    Trace.WriteLine(string.Format("         {0}: {1}", param.MySqlDbType, param.Direction == ParameterDirection.ReturnValue ? "Return" : param.Value));
                }
            }
        }
        #endregion
    }
}