﻿using System.Data;

namespace Level999.Framework.Data.Connection
{
    public interface IConnection
    {
        void GetDatabase();
        void Connect();
        void Disconnect();
        IDataReader ExecuteQuery(string query);
        int ExecuteScalar(string query);
        object ExecuteNonQuery(string cmd);
        IDataReader ExecuteNonQueryReader(string cmd);
        object GetValue(string paramName);
        object ConvertTypeToOracle(object type);
    }
}
